﻿using Confluent.Kafka;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Naruto.Subscribe.Provider.Kafka.Interface
{
    /// <summary>
    /// 生产者连接池接口
    /// </summary>
    public interface IProducerConnectionPool : IDisposable
    {
        /// <summary>
        /// 借
        /// </summary>
        /// <returns></returns>
        IProducer<string, byte[]> Rent();
        /// <summary>
        /// 还
        /// </summary>
        /// <param name="producer"></param>
        /// <returns></returns>
        bool Return(IProducer<string, byte[]> producer);
    }
}
