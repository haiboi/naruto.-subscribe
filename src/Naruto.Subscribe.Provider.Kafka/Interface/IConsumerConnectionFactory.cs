﻿using Confluent.Kafka;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Naruto.Subscribe.Provider.Kafka.Interface
{
    /// <summary>
    /// 消费者连接工厂
    /// </summary>
    public interface IConsumerConnectionFactory
    {
        /// <summary>
        /// 获取消费者
        /// </summary>
        /// <returns></returns>
        Task<IConsumer<string, byte[]>> GetAsync();

        /// <summary>
        /// 获取消费者
        /// </summary>
        /// <returns></returns>
        IConsumer<string, byte[]> Get();
    }
}
