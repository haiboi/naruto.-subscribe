﻿using Naruto.Subscribe.Object;
using Naruto.Subscribe.Provider.Kafka.Internal;
using Naruto.Subscribe.Provider.Kafka.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Subscribe.Provider.Kafka
{
    public static class KafkaSubscribeOptionExtension
    {
        /// <summary>
        /// 使用kafka订阅
        /// </summary>
        /// <param name="this"></param>
        /// <param name="option"></param>
        public static void UseKafka(this SubscribeOption @this, Action<KafkaOption> option)
        {
            @this.RegisterExtension(new SubscribeOptionExtension(option)); ;
        }
    }
}
