﻿using Microsoft.Extensions.DependencyInjection;
using Naruto.Subscribe.Interface;
using Naruto.Subscribe.Provider.Kafka.Interface;
using Naruto.Subscribe.Provider.Kafka.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Subscribe.Provider.Kafka.Internal
{
    /// <summary>
    /// kafka 配置扩展
    /// </summary>
    internal sealed class SubscribeOptionExtension : ISubscribeOptionExtension
    {
        private readonly Action<KafkaOption> option;
        public SubscribeOptionExtension(Action<KafkaOption> _option)
        {
            option = _option;
        }
        public void AddService(IServiceCollection services)
        {
            services.Configure(option);
            services.AddSingleton<IProducerConnectionPool, ProducerConnectionPool>();
            services.AddSingleton<INarutoPublish, KafkaPublish>();
            services.AddSingleton<IConsumerConnectionFactory, ConsumerConnectionFactory>();
            services.AddSingleton<IConsumerClient, KafkaConsumerClient>();
            services.AddSingleton<IConsumerClientFactory, ConsumerClientFactory>();
        }
    }
}
