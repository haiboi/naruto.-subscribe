﻿using Microsoft.Extensions.Logging;
using Naruto.Subscribe.Interface;
using Naruto.Subscribe.Provider.Kafka.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Subscribe.Provider.Kafka.Internal
{
    internal class ConsumerClientFactory : IConsumerClientFactory
    {

        /// <summary>
        /// 日志接口
        /// </summary>
        private readonly ILogger _logger;
        /// <summary>
        /// 消费者连接工厂
        /// </summary>
        private readonly IConsumerConnectionFactory _consumerConnectionFactory;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="consumerConnectionFactory"></param>
        public ConsumerClientFactory(ILogger<KafkaConsumerClient> logger, IConsumerConnectionFactory consumerConnectionFactory)
        {
            _logger = logger;
            _consumerConnectionFactory = consumerConnectionFactory;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IConsumerClient Get()
        {
            return new KafkaConsumerClient(_logger, _consumerConnectionFactory);
        }
    }
}
