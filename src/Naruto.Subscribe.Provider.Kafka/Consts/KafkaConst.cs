﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Subscribe.Provider.Kafka.Consts
{
    /// <summary>
    /// kafka的常量配置
    /// </summary>
    public class KafkaConst
    {
        /// <summary>
        /// 生产者默认的连接池
        /// </summary>
        public const int DefaultProducerConnectionPoolSize = 10;
    }
}
