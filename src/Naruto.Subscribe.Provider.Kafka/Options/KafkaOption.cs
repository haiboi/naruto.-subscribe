﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Subscribe.Provider.Kafka.Options
{
    public class KafkaOption
    {

        /// <summary>
        /// 连接地址
        /// </summary>
        public string[] BrokersServices { get; set; }

        /// <summary>
        /// 生产者连接池
        /// </summary>
        public int ProducerConnectionPoolSize { get; set; }

        /// <summary>
        /// 消费者组名称
        /// </summary>
        public string ConsumerGroup { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, string> Config { get; set; }
    }
}
