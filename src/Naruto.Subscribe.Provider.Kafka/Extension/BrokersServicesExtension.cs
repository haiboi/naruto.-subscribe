﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Subscribe.Provider.Kafka.Extension
{
    internal static class BrokersServicesExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="brokersServices"></param>
        /// <returns></returns>
        public static string ToBrokersService(this string[] brokersServices)
        {
            if (brokersServices == null)
            {
                throw new ArgumentNullException($"{nameof(brokersServices)}服务地址不能为空");
            }
            return string.Join(',', brokersServices);
        }
    }
}
