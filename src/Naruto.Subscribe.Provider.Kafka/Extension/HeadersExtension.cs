﻿using Confluent.Kafka;
using Naruto.Subscribe.Extension;
using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Subscribe.Provider.Kafka.Extension
{
    internal static class HeadersExtension
    {
        public static Headers ToHeaders(this IDictionary<string, string> headers)
        {
            if (headers == null || headers.Count <= 0)
            {
                return default;
            }
            Headers result = new Headers();
            foreach (var item in headers)
            {
                result.Add(item.Key, item.Value.ToByte());
            }
            return result;
        }
    }
}
