﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Subscribe.Provider.RabbitMQ.Consts
{
    public class RabbitMQConst
    {
        /// <summary>
        /// 生产者默认的连接池
        /// </summary>
        public const int DefaultProducerConnectionPoolSize = 10;
    }
}
