﻿using Microsoft.Extensions.Logging;
using Naruto.Subscribe.Extension;
using Naruto.Subscribe.Interface;
using Naruto.Subscribe.Object;
using Naruto.Subscribe.Provider.RabbitMQ.Interface;
using Naruto.Subscribe.Provider.RabbitMQ.Object;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Unicode;
using System.Threading;
using System.Threading.Tasks;

namespace Naruto.Subscribe.Provider.RabbitMQ
{
    /// <summary>
    /// rabbitmq消费者实现
    /// </summary>
    public class RabbitMQConsumerClient : IConsumerClient
    {
        private readonly ILogger logger;

        private readonly IConsumerChannelFactory consumerChannelFactory;

        /// <summary>
        /// 信道
        /// </summary>
        private IModel channel;

        public event EventHandler<MessageModel> OnMessageReceived;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_consumerChannelFactory"></param>
        /// <param name="_logger"></param>
        /// <param name="_subscribeHandler"></param>
        public RabbitMQConsumerClient(IConsumerChannelFactory _consumerChannelFactory, ILogger _logger)
        {
            consumerChannelFactory = _consumerChannelFactory;
            logger = _logger;
        }

        /// <summary>
        /// 释放资源
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        /// <summary>
        /// 释放资源
        /// </summary>
        /// <param name="isDispose"></param>
        protected virtual void Dispose(bool isDispose)
        {
            if (isDispose) channel?.Dispose();
        }
        public async Task SubscribeAsync(List<string> subscribeNames, CancellationToken cancellationToken = default)
        {
            subscribeNames.CheckNull();
            //创建一个信道
            channel = await consumerChannelFactory.GetAsync();
            //绑定存储的消息队列
            channel.QueueDeclare(queue: RabbitMQOption.QueueName, durable: true, exclusive: false, autoDelete: false, arguments: null);
            //设置绑定关系
            foreach (var subscribeName in subscribeNames)
            {
                logger.LogInformation("开始订阅[{subscribeName}]信息", subscribeName);
                //将队列和交换机还有路由key绑定关系
                channel.QueueBind(queue: RabbitMQOption.QueueName, exchange: RabbitMQOption.ExchangeName, routingKey: subscribeName);

                logger.LogInformation("订阅完成[{subscribeName}]", subscribeName);
            }

        }
        /// <summary>
        /// 监听
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task ListingAsync(CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();

            // 构造消费者实例
            var consumer = new EventingBasicConsumer(channel);
            // 绑定消息接收后的事件委托
            consumer.Received += ReceivedMessage;
            //消费取消
            consumer.ConsumerCancelled += ConsumerCancelledEvent;
            //服务关闭的时候
            consumer.Shutdown += ShutdownEvent;
            // 启动消费者
            channel.BasicConsume(queue: RabbitMQOption.QueueName, autoAck: false, consumer: consumer);
            return Task.CompletedTask;
        }

        /// <summary>
        /// 接收消息
        /// </summary>
        /// <param name="model"></param>
        /// <param name="ea"></param>
        protected virtual void ReceivedMessage(object model, BasicDeliverEventArgs ea)
        {
            logger.LogTrace("接收到消息,{res}", ea.Body.ToArray().ToStr());
            //获取订阅的消息
            var subscribeMsg = ea.Body.ToArray().ToStr();
            //转换对象信息
            var messageModel = subscribeMsg.ToString().ToDeserialized<MessageModel>();
            //消息头
            messageModel.Header = ea.BasicProperties?.Headers?.ToDictionary(a => a.Key, x => ((byte[])x.Value).ToStr());
            OnMessageReceived?.Invoke(ea, messageModel);
        }

        private void ConsumerCancelledEvent(object model, ConsumerEventArgs consumerEventArgs)
        {
        }

        private void ShutdownEvent(object model, ShutdownEventArgs shutdownEventArgs)
        {
            logger.LogWarning("ShutdownEvent:{shutdownEventArgs}", shutdownEventArgs.ToString());
        }
        /// <summary>
        /// 消息提交
        /// </summary>
        /// <param name="sender"></param>
        /// <returns></returns>
        public Task CommitAsync(object sender)
        {
            var ea = (BasicDeliverEventArgs)sender;
            //确认消费完此消息
            channel.BasicAck(ea.DeliveryTag, false);
            return Task.CompletedTask;
        }
        /// <summary>
        /// 消息重置
        /// </summary>
        /// <param name="sender"></param>
        /// <returns></returns>
        public Task ResetAsync(object sender)
        {
            var ea = (BasicDeliverEventArgs)sender;
            //拒绝消息 重新投递
            channel.BasicReject(ea.DeliveryTag, true);
            return Task.CompletedTask;
        }
    }
}
