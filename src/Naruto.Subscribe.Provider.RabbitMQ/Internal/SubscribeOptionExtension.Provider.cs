﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Naruto.Subscribe.Interface;
using Naruto.Subscribe.Provider.RabbitMQ.Interface;
using Naruto.Subscribe.Provider.RabbitMQ.Object;
using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Subscribe.Provider.RabbitMQ.Internal
{
    /// <summary>
    /// rabbitmq扩展
    /// </summary>
    internal sealed class SubscribeOptionExtension : ISubscribeOptionExtension
    {
        private readonly Action<NarutoRabbitMQOption> option;
        public SubscribeOptionExtension(Action<NarutoRabbitMQOption> _option)
        {
            option = _option;
        }
        public void AddService(IServiceCollection services)
        {
            services.Configure(option);
            services.TryAddSingleton<IConnectionFactory, ConnectionFactory>();
            services.TryAddSingleton<IProducerConnectionPool, ProducerConnectionPool>();
            services.TryAddSingleton<INarutoPublish, RabbitMQPublish>();
            services.TryAddSingleton<IConsumerClient, RabbitMQConsumerClient>();
            services.TryAddSingleton<IConsumerChannelFactory, ConsumerChannelFactory>();
            services.AddSingleton<IConsumerClientFactory, ConsumerClientFactory>();
        }
    }
}
