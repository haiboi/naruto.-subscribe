﻿using Microsoft.Extensions.Logging;
using Naruto.Subscribe.Interface;
using Naruto.Subscribe.Provider.RabbitMQ.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Subscribe.Provider.RabbitMQ.Internal
{
    internal class ConsumerClientFactory : IConsumerClientFactory
    {
        /// <summary>
        /// 日志
        /// </summary>
        private readonly ILogger logger;
        /// <summary>
        /// 
        /// </summary>

        private readonly IConsumerChannelFactory consumerChannelFactory;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_consumerChannelFactory"></param>
        /// <param name="_logger"></param>
        /// <param name="_subscribeHandler"></param>
        public ConsumerClientFactory(IConsumerChannelFactory _consumerChannelFactory, ILogger<RabbitMQConsumerClient> _logger)
        {
            consumerChannelFactory = _consumerChannelFactory;
            logger = _logger;
        }

        public IConsumerClient Get()
        {
            return new RabbitMQConsumerClient(consumerChannelFactory,logger);
        }
    }
}
