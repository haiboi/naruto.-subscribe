﻿using Microsoft.Extensions.Logging;
using Naruto.Subscribe.Provider.RabbitMQ.Interface;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Naruto.Subscribe.Provider.RabbitMQ.Internal
{
    /// <summary>
    /// 消费者工厂
    /// </summary>
    public class ConsumerChannelFactory : IConsumerChannelFactory
    {
        /// <summary>
        /// 管道
        /// </summary>
        private IModel model;
        /// <summary>
        /// 定义一个信号处理 保证多线程中的原子性
        /// 默认只允许一个信号
        /// </summary>
        private static readonly SemaphoreSlim _connectionSemaphoreSlim = new SemaphoreSlim(1, 1);
        /// <summary>
        /// 
        /// </summary>
        private readonly Interface.IConnectionFactory narutoConnectionFactory;

        private readonly ILogger logger;

        public ConsumerChannelFactory(Interface.IConnectionFactory _narutoConnectionFactory, ILogger<ConsumerChannelFactory> _logger)
        {
            narutoConnectionFactory = _narutoConnectionFactory;
            logger = _logger;
        }
        /// <summary>
        /// 释放资源
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        /// <summary>
        /// 释放资源
        /// </summary>
        /// <param name="isDispose"></param>
        protected virtual void Dispose(bool isDispose)
        {
            if (isDispose)
            {
                model?.Dispose();
                model = null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IModel> GetAsync()
        {
            await Connection();
            return model;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected virtual async Task Connection()
        {
            //验证消费者是否已经初始化
            if (model != null && model.IsOpen) return;

            //阻止
            await _connectionSemaphoreSlim.WaitAsync();

            try
            {
                //二次验证
                if (model != null) return;

                //初始化消费者
                model = narutoConnectionFactory.Get().CreateModel();
            }
            catch (Exception ex)
            {
                logger.LogError("消费者连接异常,error={error}", ex.Message);
                throw;
            }
            finally
            {
                //释放信号
                _connectionSemaphoreSlim.Release();
            }
        }
    }
}
