﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Naruto.Subscribe.Provider.RabbitMQ.Interface;
using Naruto.Subscribe.Provider.RabbitMQ.Object;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Naruto.Subscribe.Provider.RabbitMQ.Internal
{
    /// <summary>
    /// 连接工厂的实现
    /// </summary>
    internal class ConnectionFactory : Interface.IConnectionFactory
    {
        private readonly ILogger logger;
        /// <summary>
        /// 连接工厂对象
        /// </summary>

        public readonly global::RabbitMQ.Client.ConnectionFactory connectionFactory;

        /// <summary>
        /// 连接信息
        /// </summary>
        private IConnection connection;

        /// <summary>
        /// 定义一个信号处理 保证多线程中的原子性
        /// 默认只允许一个信号
        /// </summary>
        private static readonly SemaphoreSlim _connectionSemaphoreSlim = new SemaphoreSlim(1, 1);

        /// <summary>
        /// 配置信息
        /// </summary>
        private readonly IOptionsMonitor<NarutoRabbitMQOption> optionsMonitor;

        public ConnectionFactory(ILogger<ConnectionFactory> _logger, IOptionsMonitor<NarutoRabbitMQOption> _optionsMonitor)
        {
            logger = _logger;
            optionsMonitor = _optionsMonitor;
            //配置工厂信息
            connectionFactory = new global::RabbitMQ.Client.ConnectionFactory()
            {
                Password = optionsMonitor.CurrentValue.Password, //密码
                UserName = optionsMonitor.CurrentValue.UserName,//用户名
                Port = optionsMonitor.CurrentValue.Port,
                AutomaticRecoveryEnabled = true,//启用连接重试
                VirtualHost = "/",//虚拟主机
            };
            if (optionsMonitor.CurrentValue.HostNames.Count == 1)
            {
                connectionFactory.HostName = optionsMonitor.CurrentValue.HostNames.FirstOrDefault();
            }
        }

        public IConnection Get()
        {
            return CreateConnection();
        }
        /// <summary>
        /// 创建连接
        /// </summary>
        protected virtual IConnection CreateConnection()
        {
            if (connection != null && connection.IsOpen)
            {
                return connection;
            }
            _connectionSemaphoreSlim.Wait();
            if (connection != null && connection.IsOpen)
            {
                return connection;
            }
            try
            {
                var serviceName = Assembly.GetEntryAssembly()?.GetName().Name.ToLower();
                if (optionsMonitor.CurrentValue.HostNames.Count > 1)
                {
                    connection = connectionFactory.CreateConnection(optionsMonitor.CurrentValue.HostNames, serviceName);
                }
                else
                {
                    connection = connectionFactory.CreateConnection(serviceName);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _connectionSemaphoreSlim.Release();
            }
            return connection;
        }

        public void Dispose()
        {
            connection?.Close();
            connection?.Dispose();
        }
    }
}
