﻿using Microsoft.Extensions.Options;
using Naruto.Subscribe.Provider.RabbitMQ.Consts;
using Naruto.Subscribe.Provider.RabbitMQ.Interface;
using Naruto.Subscribe.Provider.RabbitMQ.Object;
using RabbitMQ.Client;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Naruto.Subscribe.Provider.RabbitMQ.Internal
{
    /// <summary>
    /// 生产者连接池的默认实现
    /// </summary>
    public class ProducerConnectionPool : IProducerConnectionPool
    {
        /// <summary>
        /// 生产者连接池大小 池内的最大长度
        /// </summary>
        private int _producerConnectionPoolSize;

        /// <summary>
        /// 当前池的长度
        /// </summary>
        private int _currentSize;
        /// <summary>
        ///生产者连接池
        /// </summary>
        private readonly ConcurrentQueue<IModel> _producerConnectionPool;

        /// <summary>
        /// 
        /// </summary>
        private readonly Interface.IConnectionFactory narutoConnectionFactory;

        public ProducerConnectionPool(IOptions<NarutoRabbitMQOption> options, Interface.IConnectionFactory _narutoConnectionFactory)
        {
            narutoConnectionFactory = _narutoConnectionFactory;
            _producerConnectionPoolSize = options.Value.ProducerConnectionPoolSize <= 0 ? RabbitMQConst.DefaultProducerConnectionPoolSize : options.Value.ProducerConnectionPoolSize;
            _currentSize = 0;
            _producerConnectionPool = new ConcurrentQueue<IModel>();
        }
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            Disopse(true);
        }
        /// <summary>
        /// 资源释放
        /// </summary>
        /// <param name="isDispose"></param>
        protected virtual void Disopse(bool isDispose)
        {
            if (isDispose)
            {
                _currentSize = 0;
                while (_producerConnectionPool.TryDequeue(out var connection))
                {
                    connection.Dispose();
                }
            }
        }
        /// <summary>
        /// 借
        /// </summary>
        /// <returns></returns>
        public IModel Rent()
        {
            //出队
            if (_producerConnectionPool.TryDequeue(out var producer))
            {
                Interlocked.Decrement(ref _currentSize);
                return producer;
            }

            //实例化生产者
            producer = narutoConnectionFactory.Get().CreateModel();
            return producer;
        }
        /// <summary>
        /// 还
        /// </summary>
        /// <param name="producer"></param>
        /// <returns></returns>
        public bool Return(IModel producer)
        {
            //验证当前池内是否已经满了
            if (Interlocked.Increment(ref _currentSize) > _producerConnectionPoolSize)
            {
                producer?.Dispose();
                Interlocked.Decrement(ref _currentSize);
                return false;
            }
            //入队
            _producerConnectionPool.Enqueue(producer);
            return true;
        }
    }
}
