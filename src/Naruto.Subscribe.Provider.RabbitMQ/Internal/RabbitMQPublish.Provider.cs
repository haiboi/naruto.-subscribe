﻿using Microsoft.Extensions.Logging;
using Naruto.Subscribe.Extension;
using Naruto.Subscribe.Interface;
using Naruto.Subscribe.Object;
using Naruto.Subscribe.Provider.RabbitMQ.Interface;
using Naruto.Subscribe.Provider.RabbitMQ.Object;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Naruto.Subscribe.Provider.RabbitMQ
{
    /// <summary>
    /// rabbitmq 生产者
    /// </summary>
    public class RabbitMQPublish : INarutoPublish
    {
        private readonly ILogger logger;

        private readonly IProducerConnectionPool narutoChannel;


        public RabbitMQPublish(ILogger<RabbitMQPublish> _logger, IProducerConnectionPool _narutoChannel)
        {
            narutoChannel = _narutoChannel;
            logger = _logger;
        }
        public void Publish(string subscribeName, object msg = null, IDictionary<string, string> headers = default)
        {
            //创建一个信道
            var channel = narutoChannel.Rent();
            logger.LogInformation("Publish:开始发布消息，subscribeName={subscribeName}", subscribeName);
            //转换消息
            var messageModel = new MessageModel()
            {
                Id = Guid.NewGuid().ToString(),
                MessageContent = msg?.ToJsonString()
            };
            try
            {
                channel.PublishMessage(messageModel, subscribeName, headers);
                logger.LogInformation("Publish:发布完成，subscribeName={subscribeName},messageModel={messageModel}", subscribeName, messageModel);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                narutoChannel.Return(channel);
            }
        }

        public Task PublishAsync(string subscribeName, object msg = null, IDictionary<string, string> headers = default)
        {
            TaskCompletionSource<object> taskCompletionSource = new TaskCompletionSource<object>();

            //创建一个信道
            var channel = narutoChannel.Rent();
            logger.LogInformation("Publish:开始发布消息，subscribeName={subscribeName},msg={msg}", subscribeName, msg);
            try
            {
                //转换消息
                var messageModel = new MessageModel()
                {
                    Id = Guid.NewGuid().ToString(),
                    MessageContent = msg?.ToJsonString()
                };
                channel.PublishMessage(messageModel, subscribeName, headers);
                logger.LogInformation("Publish:发布完成，subscribeName={subscribeName},messageModel={messageModel}", subscribeName, messageModel);
                taskCompletionSource.SetResult(null);
            }
            catch (Exception ex)
            {
                taskCompletionSource.SetException(ex);
                throw;
            }
            finally
            {
                narutoChannel.Return(channel);
            }
            return taskCompletionSource.Task;
        }
    }
}
