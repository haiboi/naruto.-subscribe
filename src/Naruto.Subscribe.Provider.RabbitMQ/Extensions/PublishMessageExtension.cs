﻿using Naruto.Subscribe.Extension;
using Naruto.Subscribe.Provider.RabbitMQ.Object;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naruto.Subscribe.Provider.RabbitMQ
{
    public static class PublishMessageExtension
    {

        /// <summary>
        /// 发布消息
        /// </summary>
        /// <param name="msg">消息内容</param>
        /// <param name="subscribeName">路由key</param>
        /// <param name="headers">消息头</param>
        /// <param name="channel"></param>
        public static void PublishMessage(this IModel channel, object msg, string subscribeName, IDictionary<string, string> headers = default)
        {
            // 构建byte消息数据包
            string message = msg != null ? msg.ToJsonString() : "";
            var body = Encoding.UTF8.GetBytes(message);
            //
            var basicProperties = channel.CreateBasicProperties();
            //持久化
            basicProperties.DeliveryMode = 2;
            basicProperties.Headers = headers?.ToDictionary(x => x.Key, x => (object)x.Value);
            // 发送数据包
            channel.BasicPublish(exchange: RabbitMQOption.ExchangeName, routingKey: subscribeName, basicProperties: basicProperties, body: body);
        }
    }
}
