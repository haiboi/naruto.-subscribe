﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Subscribe.Provider.RabbitMQ.Interface
{
    /// <summary>
    /// 生产者连接池
    /// </summary>
    public interface IProducerConnectionPool : IDisposable
    {
        /// <summary>
        /// 借
        /// </summary>
        /// <returns></returns>
        IModel Rent();
        /// <summary>
        /// 还
        /// </summary>
        /// <param name="producer"></param>
        /// <returns></returns>
        bool Return(IModel producer);
    }
}
