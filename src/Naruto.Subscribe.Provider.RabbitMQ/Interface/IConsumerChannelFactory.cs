﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Naruto.Subscribe.Provider.RabbitMQ.Interface
{
    /// <summary>
    /// 消费者工厂
    /// </summary>
    public interface IConsumerChannelFactory : IDisposable
    {
        /// <summary>
        /// 获取消费者管道信息
        /// </summary>
        /// <returns></returns>
        Task<IModel> GetAsync();
    }
}
