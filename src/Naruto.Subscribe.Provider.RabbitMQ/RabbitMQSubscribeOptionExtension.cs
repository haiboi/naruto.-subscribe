﻿using Naruto.Subscribe.Object;
using Naruto.Subscribe.Provider.RabbitMQ.Internal;
using Naruto.Subscribe.Provider.RabbitMQ.Object;
using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Subscribe.Provider.RabbitMQ
{
    public static class RabbitMQSubscribeOptionExtension
    {
        /// <summary>
        /// 使用mq订阅
        /// </summary>
        /// <param name="this"></param>
        /// <param name="option"></param>
        public static void UseRabbitMQ(this SubscribeOption @this, Action<NarutoRabbitMQOption> option)
        {
            @this.RegisterExtension(new SubscribeOptionExtension(option));
        }
    }
}
