﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Subscribe.Interface
{
    /// <summary>
    /// 消费者客户端工厂
    /// </summary>
    public interface IConsumerClientFactory
    {
        /// <summary>
        /// 获取消费者客户端
        /// </summary>
        /// <returns></returns>
        public IConsumerClient Get();
    }
}
