﻿using Naruto.Subscribe.Object;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Naruto.Subscribe.Interface
{
    /// <summary>
    /// 张海波
    /// 2020-09-06
    /// 订阅事件的处理
    /// </summary>
    public interface IConsumerClient : IDisposable
    {
        /// <summary>
        /// 订阅消息
        /// </summary>
        /// <param name="subscribeName">订阅的名称</param>
        /// <returns></returns>
        Task SubscribeAsync(List<string> subscribeNames, CancellationToken cancellationToken = default);

        /// <summary>
        /// 监听消息
        /// </summary>
        /// <returns></returns>
        Task ListingAsync(CancellationToken cancellationToken = default);
        /// <summary>
        /// 消息接收处理
        /// </summary>
        event EventHandler<MessageModel> OnMessageReceived;

        /// <summary>
        /// 提交
        /// </summary>
        /// <param name="sender"></param>
        /// <returns></returns>
        Task CommitAsync(object sender);

        /// <summary>
        /// 消息重置
        /// </summary>
        /// <param name="sender"></param>
        /// <returns></returns>
        Task ResetAsync(object sender);
    }
}
