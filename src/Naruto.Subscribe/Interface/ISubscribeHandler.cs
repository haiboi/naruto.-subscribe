﻿using Naruto.Subscribe.Object;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Naruto.Subscribe.Interface
{
    /// <summary>
    /// 张海波
    /// 2020-09-06
    /// 订阅的处理接口
    /// </summary>
    internal interface ISubscribeHandler
    {
        /// <summary>
        /// 处理订阅消息
        /// </summary>
        /// <param name="subscribeName">渠道 订阅名称</param>
        /// <param name="messageModel">消息内容</param>
        /// <param name="serviceProvider">服务</param>
        /// <returns></returns>
        Task HandlerAsync(string subscribeName, MessageModel messageModel,IServiceProvider serviceProvider);
    }
}
