﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Subscribe.Interface
{
    /// <summary>
    /// 配置扩展接口
    /// </summary>
    public interface ISubscribeOptionExtension
    {
        /// <summary>
        /// 注入扩展服务
        /// </summary>
        /// <param name="services"></param>
        void AddService(IServiceCollection services);
    }
}
