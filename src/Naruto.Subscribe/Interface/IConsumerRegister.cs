﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Naruto.Subscribe.Interface
{
    /// <summary>
    /// 消费者注册
    /// </summary>
    internal interface IConsumerRegister
    {
        /// <summary>
        /// 注册
        /// </summary>
        /// <returns></returns>
        Task RegisterAsync(CancellationToken cancellationToken = default);
    }
}
