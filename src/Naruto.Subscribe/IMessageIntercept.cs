﻿using Naruto.Subscribe.Object;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Naruto.Subscribe
{
    /// <summary>
    /// 消息拦截器
    /// </summary>
    public interface IMessageIntercept
    {
        /// <summary>
        /// 执行前
        /// </summary>
        /// <returns></returns>
        Task ExecuteingAsync(NarutoMessageEventData eventData);
        /// <summary>
        /// 执行后
        /// </summary>
        /// <returns></returns>
        Task ExecutedAsync(NarutoMessageEventData eventData);

        /// <summary>
        /// 消息执行报错
        /// </summary>
        /// <returns></returns>
        Task MessageErrorAsync(NarutoMessageExceptionEventData eventData);
    }
}
