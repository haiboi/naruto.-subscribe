﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Naruto.Subscribe.Interface;
using Naruto.Subscribe.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace Naruto.Subscribe
{
    /// <summary>
    /// 启用订阅
    /// </summary>
    internal class EnableSubscribeHostServices : BackgroundService
    {

        private readonly ILogger logger;

        private readonly IConsumerRegister consumerRegister;

        public EnableSubscribeHostServices(ILogger<EnableSubscribeHostServices> _logger, IConsumerRegister _consumerRegister)
        {
            logger = _logger;
            consumerRegister = _consumerRegister;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.Register(() =>
            {
                logger.LogInformation("stop");
            });
            await Task.Delay(1000 * 10).ConfigureAwait(false);
            await consumerRegister.RegisterAsync(stoppingToken).ConfigureAwait(false);
        }
    }
}
