﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Subscribe.Attributes
{
    /// <summary>
    /// 标识传递的头数据
    /// </summary>
    [AttributeUsage(AttributeTargets.Parameter)]
    public class HeaderAttribute : Attribute
    {

    }
}
