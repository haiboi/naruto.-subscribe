﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Subscribe.Extension
{
    public static class ByteExtension
    {
        /// <summary>
        /// byte转换
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static byte[] ToByte(this object obj)
        {
            if (obj == null)
            {
                return default;
            }
            return Encoding.UTF8.GetBytes(obj.ToString());
        }
        /// <summary>
        /// byte转换
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToStr(this byte[] obj)
        {
            if (obj == null)
            {
                return default;
            }
            return Encoding.UTF8.GetString(obj);
        }

    }
}
