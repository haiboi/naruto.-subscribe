﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Naruto.Subscribe;
using Naruto.Subscribe.Extension;
using Naruto.Subscribe.Interface;
using Naruto.Subscribe.Internal;
using Naruto.Subscribe.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServicesExtension
    {
        /// <summary>
        /// 添加 订阅的服务
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddSubscribeServices(this IServiceCollection services, Action<SubscribeOption> option, params Assembly[] assemblies)
        {
            SubscribeOption subscribeOption = new SubscribeOption();
            option.Invoke(subscribeOption);
            subscribeOption.ConfigureOptionExtension(services);
            services.Configure(option);
            services.AddServices(assemblies);
            return services;
        }

        /// <summary>
        /// 注入配置扩展接口
        /// </summary>
        private static void ConfigureOptionExtension(this SubscribeOption option, IServiceCollection services)
        {
            if (option.Extensions != null && option.Extensions.Count() > 0)
            {
                foreach (var item in option.Extensions)
                {
                    item.AddService(services);
                }
            }
        }
        /// <summary>
        /// 添加 订阅的服务
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        private static IServiceCollection AddServices(this IServiceCollection services, params Assembly[] assemblies)
        {
            services.TryAddSingleton<IConsumerRegister, ConsumerRegister>();
            //注入服务
            services.TryAddSingleton(typeof(DynamicMethodExpression<>));
            services.TryAddSingleton<ISubscribeHandler, SubscribeHandler>();
            services.TryAddSingleton<IMethodCache, MethodCache>();
            //设置订阅信息
            services.SetSubscribe(assemblies);
            services.AddHostedService<EnableSubscribeHostServices>();
            return services;
        }
        /// <summary>
        /// 设置订阅信息
        /// </summary>
        private static void SetSubscribe(this IServiceCollection services, params Assembly[] assemblies)
        {
            if (assemblies != null && assemblies.Count() > 0)
            {
                foreach (var item in assemblies)
                {
                    //获取需要订阅的信息
                    var list = item.GetSubscribe();
                    if (list != null && list.Count() > 0)
                    {
                        list.ForEach(subscribeType =>
                        {
                            //将订阅的信息 存储到工厂
                            SubscribeTypeFactory.Set(subscribeType);
                            //注入服务
                            services.TryAddScoped(subscribeType.ServiceType);
                        });
                    }
                }
            }
        }
    }
}
