﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Subscribe.Extension
{
    public static class JsonExtension
    {
        /// <summary>
        /// 反序列化
        /// </summary>
        /// <param name="source"></param>
        /// <param name="paramaterType"></param>
        /// <returns></returns>
        public static object ToDeserialized(this string source, Type paramaterType)
        {
            if (string.IsNullOrWhiteSpace(source))
                return source;
            if (paramaterType == typeof(string))
                return source;
            return JsonConvert.DeserializeObject(source, paramaterType);
        }
        /// <summary>
        /// json序列化
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToJsonString(this object obj)
        {
            if (obj == null)
                return default;
            return obj == null ? "" : JsonConvert.SerializeObject(obj);
        }

        /// <summary>
        /// 反序列化
        /// </summary>
        /// <param name="source"></param>
        /// <param name="paramaterType"></param>
        /// <returns></returns>
        public static T ToDeserialized<T>(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
                return default;
            return JsonConvert.DeserializeObject<T>(source);
        }
    }
}
