﻿using Naruto.Subscribe.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Subscribe.Object
{
    /// <summary>
    /// 订阅的配置信息
    /// </summary>
    public class SubscribeOption
    {
        public SubscribeOption()
        {
            ConsumerThreadCount = 1;
            Extensions = new List<ISubscribeOptionExtension>();
        }
        /// <summary>
        /// 消费者线程数默认1 
        /// </summary>
        public int ConsumerThreadCount { get; set; }

        /// <summary>
        /// 扩展信息接口
        /// </summary>
        internal List<ISubscribeOptionExtension> Extensions { get; set; }

        /// <summary>
        /// 注册扩展信息
        /// </summary>
        /// <param name="extension"></param>
        public void RegisterExtension(ISubscribeOptionExtension extension)
        {
            if (extension == null)
            {
                throw new ArgumentNullException(nameof(extension));
            }
            Extensions.Add(extension);
        }
    }
}
