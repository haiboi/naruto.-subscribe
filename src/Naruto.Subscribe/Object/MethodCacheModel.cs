﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Naruto.Subscribe.Object
{
    /// <summary>
    /// 方法缓存模型
    /// </summary>
    public class MethodCacheModel
    {
        /// <summary>
        /// 执行的方法
        /// </summary>
        public MethodInfo Method { get; set; }
        /// <summary>
        /// 方法的参数
        /// </summary>
        public ParameterInfo[] Parameters { get; set; }

        /// <summary>
        /// 是否异步
        /// </summary>
        public bool IsAsync { get; set; }
    }
}
