﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Subscribe.Object
{
    /// <summary>
    /// 消息的实体
    /// </summary>
    public class MessageModel
    {
        /// <summary>
        /// 消息的id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 消息头
        /// </summary>
        public IDictionary<string, string> Header { get; set; }
        /// <summary>
        /// 订阅的名称
        /// </summary>
        public string SubscribeName { get; set; }

        /// <summary>
        /// 消息创建时间
        /// </summary>
        public DateTime CreateTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 消息的内容
        /// </summary>
        public string MessageContent { get; set; }
    }
}
