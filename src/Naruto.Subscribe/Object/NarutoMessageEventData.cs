﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Subscribe.Object
{
    /// <summary>
    /// 消息处理事件对象
    /// </summary>
    public class NarutoMessageEventData
    {
        /// <summary>
        /// 服务 提供 者
        /// </summary>
        public IServiceProvider ServiceProvider { get; set; }

        /// <summary>
        /// 消息
        /// </summary>
        public MessageModel Message { get; set; }

    }
    /// <summary>
    /// 消息处理事件对象
    /// </summary>
    public class NarutoMessageExceptionEventData : NarutoMessageEventData
    {
        /// <summary>
        /// 异常消息
        /// </summary>
        public Exception Error { get; set; }
    }
}
