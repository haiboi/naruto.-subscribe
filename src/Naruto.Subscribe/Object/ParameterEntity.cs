﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Subscribe.Object
{
    /// <summary>
    /// 参数实体信息
    /// </summary>
    internal class ParameterEntity
    {
        /// <summary>
        /// 参数类型
        /// </summary>
        public Type ParameterType { get; set; }



        /// <summary>
        /// 参数值
        /// </summary>
        public object Value { get; set; }
    }
}
