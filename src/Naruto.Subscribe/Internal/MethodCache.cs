﻿using Naruto.Subscribe.Extension;
using Naruto.Subscribe.Interface;
using Naruto.Subscribe.Object;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Naruto.Subscribe.Internal
{
    /// <summary>
    /// 方法的缓存
    /// </summary>
    internal class MethodCache : IMethodCache
    {
        /// <summary>
        /// 缓存方法的信息
        /// </summary>
        private readonly ConcurrentDictionary<string, MethodCacheModel> methods;

        public MethodCache()
        {
            methods = new ConcurrentDictionary<string, MethodCacheModel>();
        }

        public void Dispose()
        {
            methods?.Clear();
        }

        /// <summary>
        /// 获取方法信息
        /// </summary>
        /// <param name="service"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public MethodCacheModel Get(BaseSubscribeTypeModel baseSubscribeTypeModel)
        {
            baseSubscribeTypeModel.CheckNull();
            baseSubscribeTypeModel.ServiceType.CheckNull();
            baseSubscribeTypeModel.MethodName.CheckNullOrEmpty();
            //拼接存储key
            var key = baseSubscribeTypeModel.ServiceType.Name + baseSubscribeTypeModel.MethodName;
            if (methods.TryGetValue(key, out var method))
            {
                return method;
            }
            //获取方法
            var methodInfo = baseSubscribeTypeModel.ServiceType.GetMethod(baseSubscribeTypeModel.MethodName, BindingFlags.Public | BindingFlags.Instance);
            if (methodInfo == null)
            {
                return default;
            }
            var methodCacheModel = new MethodCacheModel
            {
                Method = methodInfo,
                Parameters = methodInfo.GetParameters(),
                IsAsync = methodInfo.ReturnType == typeof(Task)
            };

            methods.TryAdd(key, methodCacheModel);
            return methodCacheModel;
        }
    }
}
