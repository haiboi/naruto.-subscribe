﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Naruto.Subscribe.Provider.Redis.Object
{
    /// <summary>
    /// 常量对象
    /// </summary>
    internal class ConstObject
    {
        /// <summary>
        /// 消息头
        /// </summary>
        internal static string Header = "header";

        /// <summary>
        /// 消息体
        /// </summary>
        internal static string Body = "body";

        /// <summary>
        /// 消费者的消息处理的缓存key'
        /// </summary>
        internal static string ConsumerMessageHanderLockKey = $"{Assembly.GetExecutingAssembly().FullName.ToLower()}_consumermessagehander:";
    }
}
