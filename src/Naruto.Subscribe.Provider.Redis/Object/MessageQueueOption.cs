﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Naruto.Subscribe.Provider.Redis.Object
{
    public class MessageQueueOption
    {
        public MessageQueueOption()
        {
            ConsumerGroup = Assembly.GetExecutingAssembly().GetName().Name;

            CountPerStream = 10;
        }
        /// <summary>
        /// 消费者组名称
        /// </summary>
        public string ConsumerGroup { get; set; }

        /// <summary>
        /// 每次读取的消息 条数
        /// </summary>
        public int CountPerStream { get; set; }
    }
}
