﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Subscribe.Provider.Redis
{
    /// <summary>
    /// 时间戳
    /// </summary>
    public static class TimeExtension
    {
        /// <summary>
        /// 日期转时间戳
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static long GetUnixTimestamp()
        {
            DateTime dateTime = DateTime.Now;
            var start = new DateTime(1970, 1, 1, 0, 0, 0, dateTime.Kind);
            return Convert.ToInt64((dateTime - start).TotalMilliseconds);
        }
    }
}
