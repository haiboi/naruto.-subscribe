﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Naruto.Subscribe.Interface;
using Naruto.Subscribe.Provider.Redis.Object;
using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Subscribe.Provider.Redis.Internal
{
    /// <summary>
    /// redis订阅扩展
    /// </summary>
    internal sealed class SubscribeOptionExtension : ISubscribeOptionExtension
    {
        private readonly Action<MessageQueueOption> option;
        public SubscribeOptionExtension(Action<MessageQueueOption> _option)
        {
            option = _option;
        }

        public void AddService(IServiceCollection services)
        {
            services.TryAddSingleton<INarutoPublish, RedisPublish>();
            services.AddSingleton<IConsumerClientFactory, ConsumerClientFactory>();
            services.Configure(option);
        }
    }
}
