﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Naruto.Redis;
using Naruto.Subscribe.Interface;
using Naruto.Subscribe.Provider.Redis.Object;
using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Subscribe.Provider.Redis.Internal
{
    internal class ConsumerClientFactory : IConsumerClientFactory
    {
        /// <summary>
        /// redis仓储
        /// </summary>
        private readonly IRedisRepository redis;
        /// <summary>
        /// 
        /// </summary>
        private readonly ILogger logger;
        /// <summary>
        /// 
        /// </summary>
        private readonly MessageQueueOption messageQueueOption;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_redis"></param>
        /// <param name="_logger"></param>
        /// <param name="_subscribeHandler"></param>
        /// <param name="_messageQueueOption"></param>
        public ConsumerClientFactory(IRedisRepository _redis, ILogger<RedisConsumerClient> _logger, IOptions<MessageQueueOption> _messageQueueOption)
        {
            redis = _redis;
            logger = _logger;
            messageQueueOption = _messageQueueOption.Value;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IConsumerClient Get()
        {
            return new RedisConsumerClient(redis, logger, messageQueueOption);
        }
    }
}
