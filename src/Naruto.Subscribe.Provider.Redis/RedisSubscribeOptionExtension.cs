﻿using Naruto.Subscribe.Object;
using Naruto.Subscribe.Provider.Redis.Internal;
using Naruto.Subscribe.Provider.Redis.Object;
using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.Subscribe.Provider.Redis
{
    public static class RedisSubscribeOptionExtension
    {
        /// <summary>
        /// 注入redis订阅
        /// </summary>
        /// <param name="this"></param>
        /// <param name="option"></param>
        public static void UseRedis(this SubscribeOption @this, Action<MessageQueueOption> option)
        {
            @this.RegisterExtension(new SubscribeOptionExtension(option));
        }
    }
}
