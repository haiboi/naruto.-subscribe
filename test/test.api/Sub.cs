﻿using Microsoft.Extensions.Logging;
using Naruto.Subscribe;
using Naruto.Subscribe.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace test.api
{
    public class Sub : ISubscribe
    {
        private readonly ILogger logger1;
        public Sub(ILogger<Sub> logger)
        {
            logger1 = logger;
        }
        [Subscribe(name: "test22")]
        public async Task Test([Header] Dictionary<string, string> keyValuePairs, string str)
        {
            logger1.LogInformation("开始处理消息,{res}", str);
            logger1.LogInformation(Thread.CurrentThread.ManagedThreadId.ToString());
            await Task.Delay(3 * 1000);
            logger1.LogInformation("消息处理完毕,{res}", str);
        }
    }

    public class sd
    {
        public string str { get; set; }
    }
}
